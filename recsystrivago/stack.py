import pandas as pd
import numpy as np
import lightgbm as lgb
from sklearn.model_selection import train_test_split
import gc
import pickle

# import ipdb
import recsystrivago.constants as constants
# import recsystrivago.constants as constants
import recsystrivago.utils as utils
import recsystrivago.ml_utils as ml_utils
import recsystrivago.preprocessing as preprocessing
# import recsystrivago.verify_submission as verify_submission
# import recsystrivago.verify as verify
import recsystrivago.feature_engineering as fe

# from sklearn.feature_extraction.text import TfidfVectorizer

df_train = pd.read_pickle('output/df_valid_pred_protoTrue.pickle')
df_valid = pd.read_pickle('output/df_valid_pred_oof_protoTrue.pickle')
df_test = pd.read_pickle('output/df_test_pred_protoTrue.pickle')

df_train = df_train.sort_values('id')
df_valid = df_valid.sort_values('id')

group_train = df_train.groupby('id').size().values
group_valid = df_valid.groupby('id').size().values

to_drop = ['id'] + ["user_id", "session_id", "timestamp", "step"]
target = 'target'

X_cols_dense = list(df_train.drop(columns=to_drop + [target]))
X_train = df_train[X_cols_dense]
X_valid = df_valid[X_cols_dense]
X_test = df_test[X_cols_dense]

y_train = df_train[target]
y_valid = df_valid[target]

categorical_feature = []

print('lgb.Dataset')
lgb_train = lgb.Dataset(
    X_train,
    label=y_train,
    group=group_train,
    categorical_feature=categorical_feature,
    # weight=weight_train,
    )
lgb_valid = lgb.Dataset(
    X_valid,
    label=y_valid,
    group=group_valid,
    categorical_feature=categorical_feature,
    # weight=weight_valid,
    )

param_ranking = {
    'objective': 'lambdarank',
    # "metric": "None",
    "metric": 'ndcg',
    'eval_at': 25,
    'random_state': 1,
    "verbosity": -1,
    'num_threads': 25,
    'num_leaves': 2**7,
    'max_position': 16,  # Doesn't seem to have any impact
    # 'min_gain_to_split': 0,
    'min_data_in_leaf': 2**8,
    'learning_rate': 0.05,
    # 'bagging_freq': 1,
    # 'feature_fraction': 0.5,
    # 'bagging_fraction': 0.7,
}

model_gbm = lgb.train(params=param_ranking,
                      train_set=lgb_train,
                      num_boost_round=6000,
                      valid_sets=[lgb_train, lgb_valid],
                      early_stopping_rounds=200,
                      # feval=ml_utils.lgb_mrr,
                      feature_name=X_cols_dense,
                      verbose_eval=50)

importance = utils.get_importance_lgb(model_gbm)

df_valid['pred'] = model_gbm.predict(X_valid)
df_valid['pred_rank'] = df_valid.groupby('id')['pred'].rank(ascending=False)

mean_reciprocial_rank = (1 / df_valid[df_valid[target] == 1]['pred_rank']).mean()
print('Valid mean_reciprocial_rank: {}'.format(mean_reciprocial_rank.round(4)))

df_test['pred'] = model_gbm.predict(X_test)
submission = fe.get_sub(df_test, test)

fname_sub = 'output/stacker_{}_proto{}.csv'.format(mean_reciprocial_rank.round(4), PROTOTYPING)
submission.to_csv(fname_sub, index=False)
