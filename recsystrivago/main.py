import pandas as pd
import numpy as np
import lightgbm as lgb
from sklearn.model_selection import train_test_split
import gc
import pickle

# import ipdb
import recsystrivago.constants as constants
# import recsystrivago.constants as constants
import recsystrivago.utils as utils
import recsystrivago.ml_utils as ml_utils
import recsystrivago.preprocessing as preprocessing
# import recsystrivago.verify_submission as verify_submission
# import recsystrivago.verify as verify
import recsystrivago.feature_engineering as fe

from sklearn.feature_extraction.text import TfidfVectorizer

PROTOTYPING = True

print('remettre clickout_cumsum_rank comme features?')
print('clickout_display_cumcount?')

item = pd.read_csv('data/item_metadata.csv')
item['item_id'] = item['item_id'].astype(str)
item['properties_cnt'] = item['properties'].str.split('|').map(len)
item['properties'] = item['properties'].fillna('')

tfidf_vectorizer = TfidfVectorizer(tokenizer=lambda x: x.split('|'), stop_words='english')
tfidf_vectorizer.fit(item['properties'])

X_cols_sparse = tfidf_vectorizer.get_feature_names()
X_cols_sparse = ['properties_' + i for i in X_cols_sparse]
print('Number of dimensions from tfidf_vectorizer', len(X_cols_sparse))

train = preprocessing.read_train(PROTOTYPING)
test = preprocessing.read_test(PROTOTYPING)

data = pd.concat([train, test])
print('Preprocessing')
data = preprocessing.add_session_cnt(data)
data = preprocessing.add_search_infos(data)
data = preprocessing.add_clickout_cumsum(data)
data['date'] = pd.to_datetime(data['timestamp'], unit='s').dt.floor('D')

print('search')
search = fe.get_search(data)

print('city')
city = fe.get_city(data)

print('fe_session')
fe_session = fe.get_fe_session(data)

print('get_fe_user_item_current_session')
fe_user_item_current_session = fe.get_fe_user_item_current_session(data)

print('get_fe_user_current_session')
fe_user_current_session = fe.get_fe_user_current_session(data)

print('clickout')
clickout = fe.get_clickout(data)

print('fe_user_item_previous_session')
fe_user_item_previous_session = fe.get_fe_user_item_previous_session(data)

print('session_search_timestamp')
session_search_timestamp = fe.get_session_search_timestamp(data)

print('display')
display = preprocessing.get_display(df=data)
display_test = display[display['reference'].isnull()].copy()
display = display[display['reference'].notnull()].copy()

display, d_label_encoder = fe.fe_display_row(display, d_label_encoder=None)
display_test, d_label_encoder = fe.fe_display_row(display_test, d_label_encoder=d_label_encoder)

print('fe_item_position')
fe_item_position = fe.get_fe_item_position(display, display_test)
# position_day = fe.get_fe_item_position_next_day(display, display_test)

print('fe_display')
fe_display = fe.get_fe_display(data)

print('viewed')
viewed = fe.get_viewed(data)

print('fe_previous_clickout')
fe_previous_clickout = fe.get_fe_previous_clickout(display, item)

print('fe_city_platform')
fe_city_platform = fe.get_fe_city_platform(data, display)

print('item_ctr')
user_fold, item_ctr = fe.get_item_ctr(pd.concat([display, display_test], sort=False), viewed)

print('get_fe_previous_interaction_prices')
previous_interaction_prices = fe.get_fe_previous_interaction_prices(data)

print('get_learning()')
df_learning = fe.get_learning(
    display=display,
    item=item,
    city=city,
    search=search,
    fe_session=fe_session,
    fe_display=fe_display,
    fe_user_item_current_session=fe_user_item_current_session,
    fe_user_current_session=fe_user_current_session,
    fe_user_item_previous_session=fe_user_item_previous_session,
    clickout=clickout,
    session_search_timestamp=session_search_timestamp,
    fe_previous_clickout=fe_previous_clickout,
    fe_city_platform=fe_city_platform,
    viewed=viewed,
    user_fold=user_fold,
    item_ctr=item_ctr,
    previous_interaction_prices=previous_interaction_prices,
    fe_item_position=fe_item_position,
    )

df_test = fe.get_learning(
    display=display_test,
    item=item,
    city=city,
    search=search,
    fe_session=fe_session,
    fe_display=fe_display,
    fe_user_item_current_session=fe_user_item_current_session,
    fe_user_current_session=fe_user_current_session,
    fe_user_item_previous_session=fe_user_item_previous_session,
    clickout=clickout,
    session_search_timestamp=session_search_timestamp,
    fe_previous_clickout=fe_previous_clickout,
    fe_city_platform=fe_city_platform,
    viewed=viewed,
    user_fold=user_fold,
    item_ctr=item_ctr,
    previous_interaction_prices=previous_interaction_prices,
    fe_item_position=fe_item_position,
    )

print('fe_learning()')
df_learning = fe.fe_learning(df_learning)
df_test = fe.fe_learning(df_test)

print('clean_data_quality_problem()')
df_learning = fe.clean_data_quality_problem(df_learning)


print('Machine learning')
id_train, id_valid = train_test_split(df_learning[['id']].drop_duplicates(), test_size=0.10, random_state=42)
df_train = pd.merge(id_train, df_learning)
df_valid = pd.merge(id_valid, df_learning)


gc.collect()

# assert we are sorted by query
df_train = df_train.sort_values('id')
df_valid = df_valid.sort_values('id')

group_train = df_train.groupby('id').size().values
group_valid = df_valid.groupby('id').size().values

to_drop = [
    'id',
    'date',
    'fold',
    'user_id',
    'session_id',
    'impressions',
    'reference',
    'item_id',
    'timestamp',
    'session_previous_interaction_timestamp',
    'previous_session_previous_interaction_timestamp',
    # 'session_cnt',
    'timestamp_search',
    'previous_clickout_step',
    'previous_clickout_timestamp',
    'fold',
    'user_fold',
    'properties',
    'city',
    'search_type',
    'search_reference',
    'current_filters',
    # 'clickout_cumsum_rank',
    'previous_item_id',
    'previous_properties',
    ]

target = 'target'

X_cols_dense = list(df_train.drop(columns=to_drop + [target]))
X_train, X_cols = fe.get_X_X_cols(df=df_train, X_cols_dense=X_cols_dense, tfidf_vectorizer=tfidf_vectorizer)
X_valid, X_cols = fe.get_X_X_cols(df=df_valid, X_cols_dense=X_cols_dense, tfidf_vectorizer=tfidf_vectorizer)
X_test, X_cols = fe.get_X_X_cols(df=df_test, X_cols_dense=X_cols_dense, tfidf_vectorizer=tfidf_vectorizer)

y_train = df_train[target]
y_valid = df_valid[target]

categorical_feature = [
    'device',
    'session_previous_interaction_action_type',
    'platform',
    # 'search_type_le',
    ]

print('lgb.Dataset')
lgb_train = lgb.Dataset(
    X_train,
    label=y_train,
    group=group_train,
    categorical_feature=categorical_feature,
    # weight=weight_train,
    )
lgb_valid = lgb.Dataset(
    X_valid,
    label=y_valid,
    group=group_valid,
    categorical_feature=categorical_feature,
    # weight=weight_valid,
    )

param_ranking = {
    'objective': 'lambdarank',
    # "metric": "None",
    "metric": 'ndcg',
    'eval_at': 25,
    'random_state': 1,
    "verbosity": -1,
    'num_threads': 25,
    'num_leaves': 2**7,
    'max_position': 16,  # Doesn't seem to have any impact
    # 'min_gain_to_split': 0,
    'min_data_in_leaf': 2**8,
    'learning_rate': 0.05,
    # 'bagging_freq': 1,
    # 'feature_fraction': 0.5,
    # 'bagging_fraction': 0.7,
}


param_regression = {
    'objective': 'regression_l2',
    "metric": 'rmse',
    'random_state': 1,
    "verbosity": -1,
    'num_threads': 25,
    'num_leaves': 2**7,
    'max_position': 16,  # Doesn't seem to have any impact
    # 'min_gain_to_split': 0,
    'min_data_in_leaf': 2**8,
    'learning_rate': 0.05,
}

param_classif = {
    'objective': 'binary',
    "metric": 'binary_logloss',
    'random_state': 1,
    "verbosity": -1,
    'num_threads': 25,
    'num_leaves': 2**7,
    'max_position': 16,  # Doesn't seem to have any impact
    # 'min_gain_to_split': 0,
    'min_data_in_leaf': 2**8,
    'learning_rate': 0.05,
}


model_gbm = lgb.train(params=param_ranking,
                      train_set=lgb_train,
                      num_boost_round=6000,
                      valid_sets=[lgb_train, lgb_valid],
                      early_stopping_rounds=200,
                      # feval=ml_utils.lgb_mrr,
                      feature_name=X_cols,
                      verbose_eval=50)

importance = utils.get_importance_lgb(model_gbm)
importance[importance.feature.str.contains('position_mean')]
# utils.plot_importance_lgb(importance.head(40).copy())
importance.to_csv('output/importance_proto{}.csv'.format(PROTOTYPING))
with open('output/model_gbm_proto{}.pickle'.format(PROTOTYPING), 'wb') as file:
    pickle.dump(model_gbm, file, protocol=pickle.HIGHEST_PROTOCOL)

df_valid['pred'] = model_gbm.predict(X_valid)
df_valid['pred_rank'] = df_valid.groupby('id')['pred'].rank(ascending=False)

mean_reciprocial_rank = (1 / df_valid[df_valid[target] == 1]['pred_rank']).mean()
print('Valid mean_reciprocial_rank: {}'.format(mean_reciprocial_rank.round(4)))

df_test['pred'] = model_gbm.predict(X_test)
submission = fe.get_sub(df_test, test)

fname_sub = 'output/submission_{}_proto{}.csv'.format(mean_reciprocial_rank.round(4), PROTOTYPING)
submission.to_csv(fname_sub, index=False)

print('End of script')  # noqa
