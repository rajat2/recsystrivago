import pandas as pd
import numpy as np
import math

import plotnine as pn
from plotnine import ggplot, aes  # noqa
from plotnine.geoms import *  # noqa
coef = 1.5
pn.options.figure_size = (6.4*coef, 4.8*coef)
from mizani.formatters import percent_format  # noqa
# from mizani.breaks import date_breaks  # noqa
# from mizani.formatters import date_format  # noqa


def reduce_mem_usage(df, verbose=True):
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    start_mem = df.memory_usage().sum() / 1024**2
    for col in df.columns:
        col_type = df[col].dtypes
        if col_type in numerics:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)
    end_mem = df.memory_usage().sum() / 1024**2
    if verbose:
        print('Mem. usage decreased to {:5.2f} Mb ({:.1f}% reduction)'.format(end_mem, 100 * (start_mem - end_mem) / start_mem))
    return df


def string_to_array(s):
    """Convert pipe separated string to array."""

    if isinstance(s, str):
        out = s.split("|")
    elif math.isnan(s):
        out = []
    else:
        raise ValueError("Value must be either string of nan")
    return out


def explode(df_in, col_expl):
    """Explode column col_expl of array type into multiple rows."""

    df = df_in.copy()
    df.loc[:, col_expl] = df[col_expl].apply(string_to_array)

    df_out = pd.DataFrame(
        {col: np.repeat(df[col].values,
                        df[col_expl].str.len())
         for col in df.columns.drop(col_expl)}
    )

    df_out.loc[:, col_expl] = np.concatenate(df[col_expl].values)
    df_out.loc[:, col_expl] = df_out[col_expl]  # .apply(int)

    return df_out


def get_importance_lgb(model_gbm):
    importance = pd.DataFrame()
    importance["feature"] = model_gbm.feature_name()
    importance["importance"] = model_gbm.feature_importance(importance_type='gain')
    importance['importance'] = importance['importance'] / importance['importance'].sum()
    importance['importance'] = importance['importance'] * 100
    importance['importance_rank'] = importance['importance'].rank(ascending=False).astype(int)
    importance = importance.sort_values('importance_rank')  # .round(5)
    return importance


def plot_importance_lgb(importance):
    importance['importance'] = importance['importance'] / 100
    importance['feature'] = pd.Categorical(
        importance['feature'],
        importance['feature'][::-1], ordered=True)
    plot = (ggplot(importance, aes('feature', 'importance')) +
                 geom_bar(stat='identity') +
                 pn.coords.coord_flip()+
                 pn.scales.scale_y_continuous(labels=percent_format()) +
                 pn.labs(title='Feature importance', x='Feature', y='Gain'))
    return plot


def adversarial_validation(X_train, X_test, X_cols=None):
    from sklearn.model_selection import train_test_split
    import lightgbm as lgb
    import numpy as np
    import scipy
    from scipy.sparse import vstack
    if isinstance(X_train, pd.DataFrame):
        X_full = X_train.append(X_test)
        X_cols = list(X_train)
    if isinstance(X_train, scipy.sparse.csr.csr_matrix):
        X_full = vstack([X_train, X_test], format='csr')

    y_full = np.concatenate((np.ones(X_train.shape[0]), np.zeros(X_test.shape[0])))

    X_train, X_valid, y_train, y_valid = train_test_split(X_full, y_full)

    param = {'objective': 'binary',
             "metric": 'auc',
             # 'learning_rate': 0.1,
             }

    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid)

    model_gbm = lgb.train(param, lgb_train, 200,
                          valid_sets=[lgb_train, lgb_valid],
                          early_stopping_rounds=10,
                          feature_name=X_cols,
                          verbose_eval=50)

    return model_gbm
