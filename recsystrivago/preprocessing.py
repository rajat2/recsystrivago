import dask.dataframe as dd
import pandas as pd
import numpy as np

import recsystrivago.utils as utils
import recsystrivago.constants as constants


def read_data(f, PROTOTYPING):
    data = dd.read_csv(f)

    if PROTOTYPING:
        data = data[data['user_id'].str[0:1] == '0'].copy()

    data = data.compute()
    col_id = data['user_id'] + data['session_id'] + data['step'].astype(str)
    data.insert(0, 'id', col_id)

    return data


def read_train(PROTOTYPING):
    return read_data('data/train.csv', PROTOTYPING)


def read_test(PROTOTYPING):
    return read_data('data/test.csv', PROTOTYPING)


def get_session_cnt(data):
    session_cnt = data[['user_id', 'session_id']].drop_duplicates()
    session_cnt['session_cnt'] = (
        session_cnt.eval('session_cnt = 1')
        .groupby(['user_id'])['session_cnt'].cumsum())
    return session_cnt


def add_session_cnt(data):
    session_cnt = get_session_cnt(data)
    nrow_begin = data.shape[0]
    data = pd.merge(data, session_cnt)
    nrow_end = data.shape[0]
    assert nrow_begin == nrow_end
    return data


def add_search_infos(data):
    # When action is search, add search_type
    data['search_type'] = np.where(
        data['action_type'].map(constants.D_ACTION_TYPE) == 'search',
        data['action_type'],
        np.nan
    )
    # When action is search, addd reference
    data['search_reference'] = np.where(
        data['action_type'].map(constants.D_ACTION_TYPE) == 'search',
        data['reference'],
        np.nan
    )

    # detect new display
    is_filter_selection = data['action_type'] == 'filter selection'
    is_change_order = (
        (data['action_type'] == 'change of sort order') &
        (data['reference'] != 'interaction sort button'))  # Click but no action
    is_search = data['action_type'].map(constants.D_ACTION_TYPE) == 'search'
    is_new_display = is_filter_selection | is_change_order | is_search
    data['impressions'] = np.where(
        is_new_display,
        'new display',
        data['impressions'])

    # Forward/backward fill now
    grouped = data.groupby(['user_id', 'session_id'])
    data['search_type'] = grouped['search_type'].ffill()
    data['search_reference'] = grouped['search_reference'].ffill()
    data['impressions'] = data.groupby(['session_id'])['impressions'].bfill()
    data['prices'] = data.groupby(['session_id'])['prices'].bfill()
    return data


def add_clickout_cumsum(data):
    # Add clickout_rank, to know if it is the last one or not
    cols = ['user_id', 'session_id']
    # assert sorted by time
    data['timestamp_clickout'] = np.where(
        data['action_type'] == 'clickout item',
        data['timestamp'],
        np.nan
    )
    grouped = data.groupby(cols)
    data['clickout_cumsum'] = (
        grouped['timestamp_clickout'].rank(method='first'))
    data['clickout_cumsum_rank'] = (
        grouped['timestamp_clickout']
        .rank(method='first', ascending=False))
    return data


def get_display(df):
    cols = ['id',
            'date',
            'user_id',
            'session_id',
            'session_cnt',
            'timestamp',
            'step',
            'impressions',
            'prices',
            'reference',
            'city',
            'search_type',
            'search_reference',
            'current_filters',
            'clickout_cumsum',
            'clickout_cumsum_rank',
            ]
    df = df[df["action_type"] == "clickout item"]
    df = df[cols]
    impressions = utils.explode(df[['id', 'impressions']], 'impressions')
    impressions = impressions.rename(columns={'impressions': 'item_id'})

    # FE impressions
    impressions['position'] = 1
    grouped = impressions.groupby('id')
    impressions['position'] = grouped['position'].cumsum()
    impressions_cnt = grouped['position'].size().rename('impressions_cnt').reset_index()
    impressions = pd.merge(impressions, impressions_cnt)

    # Add price
    prices = utils.explode(df[['id', 'prices']], 'prices')
    prices = prices.rename(columns={'prices': 'price'})
    prices['price'] = prices['price'].astype(int)
    impressions['price'] = prices['price']

    display = pd.merge(df.drop(columns=['prices']), impressions)
    # display['search_reference_is_item_id'] = (
    #     display['item_id'] == display['search_reference']).astype(int)
    display['target'] = display['reference'] == display['item_id']
    display['target'] = display['target'].astype(int)

    return display
