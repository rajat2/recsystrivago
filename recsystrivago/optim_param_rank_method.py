"""
Standalone script to get the optimal parameter for ranking method ['average', 'min', 'max']
in feature engineering.
"""


def fe_learning(df_learning, method):
    df_learning['user_id_session_id_reference_step_timestamp_dist'] = df_learning['timestamp'] - df_learning['user_id_session_id_reference_step_timestamp']
    df_learning['diff_last_search'] = df_learning['timestamp'] - df_learning['timestamp_search']
    df_learning['position_prev_disp_inter_display_position_diff'] = df_learning['position'] - df_learning['prev_disp_inter_display_position']

    grouped = df_learning.groupby('id')
    to_rank_ascending = ['price']
    # Warning by default ascending = True
    for col in to_rank_ascending:
        df_learning['{}_rank'.format(col)] = grouped[col].rank()

    to_rank_descending = [
        'user_id_session_id_reference_step_interaction_cnt',
        'position_prev_disp_inter_display_position_diff',
        'user_id_session_id_reference_step_timestamp_dist',
        'user_id_reference_session_cnt_interaction_cnt',
        # 'properties_cnt',
        ]

    # TODO : hyperparameter optim of method
    for col in to_rank_descending:
        df_learning['{}_rank'.format(col)] = grouped[col].rank(
            ascending=False,
            na_option='bottom',
            method=method,
            )

    return df_learning


def get_score(df_learning, method):
    print('fe_learning()')
    df_learning = fe_learning(df_learning, method)
    # df_test = fe_learning(df_test, method)

    print('clean_data_quality_problem()')
    df_learning = fe.clean_data_quality_problem(df_learning)

    print('Machine learning')
    # Split by group/query
    id_train, id_valid = train_test_split(df_learning[['id']].drop_duplicates(), test_size=0.33, random_state=42)
    df_train = pd.merge(id_train, df_learning)
    df_valid = pd.merge(id_valid, df_learning)

    # assert we are sorted by query
    df_train = df_train.sort_values('id')
    df_valid = df_valid.sort_values('id')

    group_train = df_train.groupby('id').size().values
    group_valid = df_valid.groupby('id').size().values


    to_drop = [
        'id',
        'user_id',
        'session_id',
        'platform',
        'city',
        # 'device',
        'reference',
        'item_id',
        'properties',
        'timestamp',
        'user_id_session_id_reference_step_timestamp',
        'user_id_reference_session_cnt_timestamp',
        'session_cnt',
        'timestamp_search',
        ]
    target = 'target'

    X_train = df_train.drop(columns=to_drop + [target], errors='ignore')
    y_train = df_train[target]
    X_valid = df_valid.drop(columns=to_drop + [target], errors='ignore')
    y_valid = df_valid[target]
    X_cols = list(X_train)
    # X_test = df_test[X_cols]


    lgb_train = lgb.Dataset(X_train, label=y_train, group=group_train)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid, group=group_valid)


    param = {
        'objective': 'binary',
        # "metric": 'logloss',
        # 'learning_rate': learning_rate,
        'random_state': 1,
        "verbosity": -1,
    }


    param_ranking = {
        'objective': 'lambdarank',
        # "metric": ['ndcg@5'],
        'eval_at': 5,
        'random_state': 1,
        "verbosity": -1,
    }

    model_gbm = lgb.train(params=param_ranking,
                          train_set=lgb_train,
                          num_boost_round=2000,
                          valid_sets=[lgb_train, lgb_valid],
                          early_stopping_rounds=50,
                          # categorical_feature=categorical_feature,
                          # feval=ml_utils.lgb_mrr,
                          feature_name=X_cols,
                          verbose_eval=50)

    importance = utils.get_importance_lgb(model_gbm)
    # importance[importance.feature.str.contains('properties_cnt')]
    importance.feature.values

    df_valid['pred'] = model_gbm.predict(X_valid)
    df_valid['pred_rank'] = df_valid.groupby('id')['pred'].rank(ascending=False)
    mean_reciprocial_rank = (1 / df_valid[df_valid[target] == 1]['pred_rank']).mean()
    print('Valid mean_reciprocial_rank: {}'.format(mean_reciprocial_rank.round(4)))
    return mean_reciprocial_rank


l_method = ['average', 'min', 'max']
mean_reciprocial_rank = []
for method in l_method:
    mean_reciprocial_rank.append(get_score(df_learning, method))


optim_param_rank_method_result = pd.DataFrame()
optim_param_rank_method_result['method'] = l_method
optim_param_rank_method_result['mrr'] = mean_reciprocial_rank
optim_param_rank_method_result = optim_param_rank_method_result.sort_values('mrr', ascending=False)
optim_param_rank_method_result.to_csv('output/optim_param_rank_method_result.csv', index=False)

best_param = optim_param_rank_method_result['method'].values[0]
print('Best parameters for ranking is : {}'.format(best_param))
# It is min and it is quite logical.
