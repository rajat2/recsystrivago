import pandas as pd
import numpy as np


def date_fe(df, col):
    """
    df a Dataframe
    col : a string - a date columns on which we want to apply diverse func
    TODO to continue
    """
    # df['{}_year'.format(col)] = df[col].dt.year
    # df['{}_month'.format(col)] = df[col].dt.month
    # df['{}_day'.format(col)] = df[col].dt.day
    # df['{}_dayofweek'.format(col)] = df[col].dt.dayofweek
    df['{}_hour'.format(col)] = df[col].dt.hour
    # df['{}_minute'.format(col)] = df[col].dt.minute
    # df['{}_second'.format(col)] = df[col].dt.second
    return df


def adversarial_validation(X_train, X_test):
    """
    Train a lightgbm model to try identifying differences (leak) betwen a
    training and test set
    """
    from sklearn.model_selection import train_test_split
    import lightgbm as lgb
    import numpy as np
    import scipy
    from scipy.sparse import vstack
    if isinstance(X_train, pd.DataFrame):
        X_full = X_train.append(X_test)
    if isinstance(X_train, scipy.sparse.csr.csr_matrix):
        X_full = vstack([X_train, X_test], format='csr')

    y_full = np.concatenate((np.ones(X_train.shape[0]), np.zeros(X_test.shape[0])))

    X_train, X_valid, y_train, y_valid = train_test_split(X_full, y_full)

    param = {'objective': 'binary',
             "metric": 'auc',
             'learning_rate': 0.01}

    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid)

    model_gbm = lgb.train(param, lgb_train, 200,
                          valid_sets=[lgb_train, lgb_valid],
                          early_stopping_rounds=10,
                          verbose_eval=10)

    return model_gbm


def save_metric(df_metric, path='output/df_metric.csv'):
    df_metric['timestamp'] = pd.Timestamp.today().round("S")
    try:
        df_metric_old = pd.read_csv(path)
        df_metric = pd.concat([df_metric_old, df_metric], sort=True)
    except Exception as e:
        print(e)

    df_metric = df_metric.drop_duplicates()
    df_metric.to_csv(path, index=False)

    print('df_metric saved.')
    return None


def target_embedding(data, col, target):
    # Remove line where target is Na otherwise count values wouldn't be good
    data = data[~data[target].isnull()].copy()
    data[col] = data[col].fillna('dont_drop_na')
    new_col = '_'.join(col)
    # res = pd.DataFrame()
    grouped = data.groupby(col)
    res = grouped.agg({target: ['mean', 'count']})
    res.columns = [new_col + '_' + '_'.join(c).strip() for c in res.columns]
    res = res.reset_index()
    # res = grouped[target].mean().rename('{}_{}_mean'.format(new_col, target)).reset_index()
    # res['{}_{}_count'.format(new_col, target)] = grouped.size().values
    res = res.replace('dont_drop_na', np.nan)
    return res


def target_embedding_smooth(data, col, target, f=1, k=10, prior_col=None):
    # Copy/paste from target_embedding
    data = data[~data[target].isnull()].copy()
    data[col] = data[col].fillna('dont_drop_na')
    new_col = '_'.join(col)
    col_mean = '{}_{}_mean'.format(new_col, target)
    col_cnt = '{}_{}_count'.format(new_col, target)
    col_smoothed = '{}_{}_smooth_f{}_k{}'.format(new_col, target, f, k)
    res = pd.DataFrame()
    grouped = data.groupby(col)
    res = grouped[target].mean().rename(col_mean).reset_index()
    res[col_cnt] = grouped.size().values
    res = res.replace('dont_drop_na', np.nan)

    # Start easy, but there are different options for prior :
    # on all datapoints
    # mean of group (each group col have the same weight)
    # one of the columns of col, if there is a hierarchy (eg: cols = ['region', 'city'], prior can be based on the corresponding region)
    if prior_col is None:
        prior = data[target].mean()
        res['prior'] = prior
    else:
        # Not ready yet :(
        prior = data.groupby(prior_col)[target].mean().rename('prior').reset_index()
        prior = prior.fillna(data[target].mean())  # Safety
        res = pd.merge(res, prior)

    res['smooth'] = 1 / (1 + np.exp(-1 * (res[col_cnt] - k) / f))
    res[col_smoothed] = res['smooth'] * res[col_mean] + (1-res['smooth']) * res['prior']
    res = res[col + [col_smoothed]]

    return res


def oof_dataframe(data, col_fold, function, **kwargs):
    folders = data[col_fold].unique()
    res = pd.DataFrame()
    for f in folders:
        res_tmp = function(data[data[col_fold] != f].copy(), **kwargs)
        res_tmp[col_fold] = f
        res = res.append(res_tmp)

    return res


def lgb_mrr(preds, data):
    """
    Eval metric for Mean Reciprocial Rank with LightGbm
    Slower than using nDGC and seems rather close.
    """
    label = data.get_label()
    label = np.array(label)
    group = data.get_group()
    # import ipdb
    # ipdb.set_trace()
    start_g = 0
    # cnt = 0
    mrr = np.zeros_like(group)
    mrr = mrr.astype(float)
    for i, g in enumerate(group):
        end_g = start_g + g
        label_group = label[start_g:end_g]
        pred_group = preds[start_g:end_g]
        # inds = p.argsort()
        pred_group_order = pred_group.argsort()[::-1]
        # order = array.argsort()
        pred_group_rank = pred_group_order.argsort()
        rr = 1 / (pred_group_rank[label_group == 1][0] + 1)
        mrr[i] = rr
        # if i == 9:
        #     import ipdb
        #     ipdb.set_trace()
        start_g += g

    mrr = mrr.mean()
    return 'mrr', mrr, True


def _hstack_dense_sparse(X_dense, X_sparse, X_cols_sparse):
    """
    Returns a sparse matrix ready for LightGbm
    from a dense (pandas DataFrame) and a sparse matrix
    """
    from scipy.sparse import hstack
    X = hstack([X_dense, X_sparse], format='csr')
    X_cols = list(X_dense) + X_cols_sparse
    return X, X_cols


def hstack_dense_sparse(X_dense, X_sparse, X_cols_sparse, SIZE_CHUNK=None):
    """
    Returns a sparse matrix ready for LightGbm
    from a dense (pandas DataFrame) and a sparse matrix
    SIZE_CHUNK allows to reduce peak memory
    """
    from scipy.sparse import vstack
    if SIZE_CHUNK is None:
        X, X_cols = _hstack_dense_sparse(X_dense, X_sparse, X_cols_sparse)
    else:
        l_X = []
        for i in range(0, X_dense.shape[0], SIZE_CHUNK):
            # print('i : {}'.format(i))
            X_tmp, X_cols = _hstack_dense_sparse(X_dense[i:i+SIZE_CHUNK], X_sparse[i:i+SIZE_CHUNK], X_cols_sparse)
            l_X.append(X_tmp)

        X = vstack(l_X)

    return X, X_cols
