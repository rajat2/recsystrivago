# Define dictionnary of different action_type
l_action_type = [
    'search for poi',
    'interaction item image',
    'clickout item',
    'interaction item info',
    'interaction item deals',
    'search for destination',
    'filter selection',
    'interaction item rating',
    'search for item',
    'change of sort order',
    ]
D_ACTION_TYPE = {}
for v in l_action_type:
    if 'search' in v:
        D_ACTION_TYPE[v] = 'search'
    elif 'interaction' in v:
        D_ACTION_TYPE[v] = 'interaction'
    else:
        D_ACTION_TYPE[v] = v
